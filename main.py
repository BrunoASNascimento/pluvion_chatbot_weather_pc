import requests
import json
from datetime import datetime, timedelta
from flask import jsonify
import numpy as np
import pluvion_chatbot_scale as scale

power_idw = 2


def config_date(day_back):
    #(dateNow, dateBefore, dateOneHour, datePcIntNow) = config_date()
    dateNow = datetime.utcnow()-timedelta(days=day_back)
    dateBefore = dateNow+timedelta(days=1)
    dateOneHour = dateNow-timedelta(hours=1)
    datePcIntNow = dateNow-timedelta(minutes=5)
    return (dateNow, dateBefore, dateOneHour, datePcIntNow)


def idw(data_idw, power):
    # data_idw = [[value1,dist1],[value2,dist2]...[valuen,distn]]
    numerator = []
    denominator = []
    for data in data_idw:
        if (data[1] == 0):
            return data[0]
        numerator.append(data[0]/(data[1]**power))
        denominator.append(1/(data[1]**power))
    z = sum(numerator)/sum(denominator)
    return z


def getStations(lat, lon):
    url = "https://us-central1-pluvion-tech.cloudfunctions.net/pluvion_chatbot_closest_stations/"
    querystring = {"lat": lat, "lon": lon}
    response = requests.request("GET", url, params=querystring)
    data = json.loads(response.text)
    data = data['data']
    return data


def getDataWeather(station, day_back):
    (dateNow, dateBefore, dateOneHour, datePcIntNow) = config_date(day_back)
    url = "https://us-central1-pluvion-tech.cloudfunctions.net/tools_fs/get"
    value = (
        str(station) +
        dateNow.strftime('-%Y%m%d_030000') +
        dateBefore.strftime('-%Y%m%d_025959')
    )
    print(value)
    payload = {"collectionName": "msg_summary",
               "filters":
               {
                   "where": [
                       {"field": "sumCode",
                        "op": "==",
                        "value": value
                        }
                   ]
               }
               }
    headers = {
        'Content-Type': "application/json",
        'Host': "us-central1-pluvion-tech.cloudfunctions.net",
        'Accept-Encoding': "gzip, deflate",
        'Content-Length': "251",
        'Connection': "keep-alive",
        'cache-control': "no-cache"
    }

    response = requests.request(
        "POST", url, data=json.dumps(payload), headers=headers)
    data = json.loads(response.text)
    #print('Data: ', data)
    try:
        data = data['data']
    except:
        data = []
    return data


def arrayDataWeather(lat, lon):
    stations = getStations(lat, lon)
    data = []
    for station in stations:
        weather = getDataWeather(station[0], 0)
        if weather != []:
            data.append([weather, station[1]])
        else:
            data.append([getDataWeather(station[0], 1), station[1]])
    # print(len(data))
    return data


def dataParserAll(lat, lon):
    (dateNow, dateBefore, dateOneHour, datePcIntNow) = config_date(0)

    data = arrayDataWeather(lat, lon)

    pc_last = []
    pc_VolOneHour = []
    pc_VolNow = []
    pc_int = []

    for data_parter in data:
        if (data_parter[0] != []):
            haversine = data_parter[1]

            pcVolOneHour = 0
            pcVolNow = 0
            time_int = []
            try:
                for msg in data_parter[0][0]['pc']['msgs']:

                    date_measuredAt = datetime.utcfromtimestamp(
                        msg['measuredAt']
                    )

                    if(date_measuredAt >= dateOneHour):
                        pcVolOneHour = pcVolOneHour + msg['pcVol']

                    if(date_measuredAt >= datePcIntNow):
                        pcVolNow = pcVolNow + msg['pcVol']
                        time_int.append(datetime.strptime(
                            msg['measuredAtISO'], '%Y-%m-%dT%H:%M:%S.%fZ'))

                last_measuredAt = datetime.utcfromtimestamp(
                    data_parter[0][0]['pc']['last']['measuredAt'])

                pc_last.append(
                    [data_parter[0][0]['pc']['last']['pcVol'], haversine]
                )
                pc_VolOneHour.append(
                    [pcVolOneHour, haversine]
                )
                pc_VolNow.append(
                    [pcVolNow, haversine]
                )

                pc_int_num = (
                    (60*pcVolNow) /
                    (
                        (
                            datetime.utcnow()-np.min(time_int)
                        )//timedelta(minutes=1))
                )

                pc_int.append(
                    [pc_int_num, haversine]
                )

            except:
                pc_last.append(
                    [0, haversine]
                )
                pc_VolOneHour.append(
                    [0, haversine]
                )
                pc_VolNow.append(
                    [0, haversine]
                )
                pc_int.append(
                    [0, haversine]
                )
                last_measuredAt = dateNow

    pc = {
        "server_date": dateNow,
        "last_measuredAt": last_measuredAt,
        "pc_VolOneHour": scale.pc_vol(idw(pc_VolOneHour, power_idw)),
        "pc_VolNow":  scale.pc_vol(idw(pc_VolNow, power_idw)),
        "pc_last":  scale.pc_vol(idw(pc_last, power_idw)),
        "pc_int":  scale.pc_int(idw(pc_int, power_idw))
    }

    data = {
        "status": True,
        "data":
            {
                "pc": pc
            }
    }
    print(data)
    return data


def pluvion_chatbot_weather_pc(request):
    json_req = request.args.to_dict(flat=False)
    # url/?lat=value&lon=value
    lat = round(float((json_req['lat'])[0]), 3)
    lon = round(float((json_req['lon'])[0]), 3)
    print(lat, lon)
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for the main request
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    try:
        data = dataParserAll(lat, lon)
    except:
        data = {"status": False}

    return (jsonify(data), 200, headers)


# dataParserAll(-23.55367, -46.65934)
# dataParserAll(-23.558989, -46.65958)
